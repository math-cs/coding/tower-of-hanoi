#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""TOWER OF HANOI SOLVERS, by Ehssan
Shows two recursive solutions to the puzzle."""

# First Solution
a = [3, 2, 1]
b = []
c = []


def display_board(a, b, c):
    print(f'pole a: {str(a):<15} pole b: {str(b):<15} pole c: {str(c):<15}')


def solver1(n_disks, start, end, temp):
    if n_disks == 1:
        # Base case
        end.append(start.pop())
        display_board(a, b, c)
    else:
        # Recursive case
        solver1(n_disks - 1, start, temp, end)
        end.append(start.pop())
        display_board(a, b, c)
        solver1(n_disks - 1, temp, end, start)


print('Solution 1:')
solver1(3, a, b, c)


# Second Solution


class Board:
    def __init__(self, n_disks):
        self.a = [i for i in range(n_disks, 0, -1)]
        self.b = []
        self.c = []

    def _move(self, start, end):
        # Move one disk from the start to the end pole and display the board.
        getattr(self, end).append(getattr(self, start).pop())
        self.print_board()

    def print_board(self):
        print(
            f'pole a: {str(self.a):<15} pole b: {str(self.b):<15} pole c: {str(self.c):<15}')


def solver2(a, b, c, board):
    if len(a) + len(b) + len(c) == 1:
        # Base case
        if len(a) == 1:
            b = [a.pop()]
            board._move('a', 'b')
        elif len(b) == 1:
            c = [b.pop()]
            board._move('b', 'c')
        else:
            a = [c.pop()]
            board._move('c', 'a')
    else:
        # Recursive case
        if a:
            temp_a = a[1:]
            a = a[0:1]
            temp_a, temp_b, temp_c = solver2(temp_a, b, c, board)
            if temp_b:
                c = [a.pop()]
                board._move('a', 'c')
            else:
                b = [a.pop()]
                board._move('a', 'b')
        elif b:
            temp_b = b[1:]
            b = b[0:1]
            temp_a, temp_b, temp_c = solver2(a, temp_b, c, board)
            if temp_a:
                c = [b.pop()]
                board._move('b', 'c')
            else:
                a = [b.pop()]
                board._move('b', 'a')
        else:
            temp_c = c[1:]
            c = c[0:1]
            temp_a, temp_b, temp_c = solver2(a, b, temp_c, board)
            if temp_b:
                a = [c.pop()]
                board._move('c', 'a')
            else:
                b = [c.pop()]
                board._move('c', 'b')
        temp_a, temp_b, temp_c = solver2(temp_a, temp_b, temp_c, board)
        c.extend(temp_c)
        b.extend(temp_b)
        a.extend(temp_a)
    return a, b, c


board = Board(3)
print('Solution 2:')
solver2([3, 2, 1], [], [], board)
